# SIFT

Owner, Co-developer: Mehul Gupta - @mehuld@utexas.edu

Co-developer: Billy Anderson

## Getting started

To run SIFT locally you will need to install [Docker](https://www.docker.com/products/docker-desktop/) and either [MAKE](https://gnuwin32.sourceforge.net/packages/make.htm) or Python.

## Running with Docker and Python (Recommended)

In the main SIFT folder run pyDockerRunner.py and view the site at the url: http://localhost:5000/

## Running with Docker and Make

To run SIFT first create the docker image with the following make command
```bash
make docker_image
```

Then you can run just the application with the following make command and view it at the url: http://localhost:5000/
```bash
make docker_run
```

Or if you are in windows you can run the following command to launch it in your terminal and view the website at http://localhost:5000/:
```bash
make docker_terminal_windows
```

If you need to remake the image or you are done working and want to clear up space run the following make command:
```bash
make docker_remove_image
```

## Backend Physics Equations - Compressible Orifice

Compressible orifice flowrate and diameter are solved and returned using the following equation

$\dot{m} = Cd A P_1 \sqrt{\frac{g \cdot \gamma}{R \cdot T_1} \cdot \left(\frac{2}{{\gamma + 1}}\right)^{\frac{\gamma + 1}{\gamma - 1}}}$
for choked sonic flow - Eq 3.8.2.3g in the aerospace fluids components handbook

$\dot{m} = Cd A \rho_1 \left(\frac{P_2}{{P_1}}\right)^{\frac{1}{\gamma}} \sqrt{2 g R T_1 \cdot \frac{\gamma}{\gamma - 1} \cdot \left[1 - \left(\frac{P_2}{{P_1}}\right)^{\frac{\gamma - 1}{\gamma}} \right] }$
for unchoked subsonic flow - Eq 3.8.2.3a in the aerospace fluids components handbook

$Cv = CdA * 38$
this approximation is used for both compressible and incompressible flow

## Backend Physics Equations - Incompressible Orifice

Incompressible orifice flowrate and diameter are solved and returned using the following equation

$\dot{m} = Cd A \rho_1 \sqrt{2g \frac{\Delta P}{\rho_1 g}}$
Eq 3.8.2.2b in the aerospace fluids components handbook

$Cv = CdA * 38$
this approximation is used for both compressible and incompressible flow

## Heroku deployment

```bash
heroku container:push web --app sift
heroku container:release web --app sift
```

If the above doesn't work, try

```bash
docker build --platform linux/amd64 -t registry.heroku.com/sift/web .
docker push registry.heroku.com/sift/web
heroku container:release web --app sift
```
