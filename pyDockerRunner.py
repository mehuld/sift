import docker
import threading
from pathlib import Path

def check_docker_image_exists(client, image_name):
    try:
        # Check if the image exists
        image = client.images.get(image_name)
        return True
    except docker.errors.ImageNotFound:
        # Image does not exist
        return False
    except docker.errors.APIError as e:
        # Handle other Docker API errors here if needed
        print(f"Error: {str(e)}")
        return False
    
def build_docker_image(client, image_name, dockerfile_path='.'):
    try:
        # Build the Docker image from the current directory
        image, build_logs = client.images.build(
            path=dockerfile_path,
            tag=image_name,
        )

        for log_line in build_logs:
            # Print build logs if needed
            print(log_line)

        return image
    except docker.errors.APIError as e:
        # Handle Docker API errors here if needed
        print(f"Error: {str(e)}")
        return None
    
def run_docker_container(client, image_name, container_name, port_mapping):
    try:
        cwd = Path.cwd()
        app_dir = cwd / "app"
        fluid_dir = cwd / "fluids"
        unit_dir = cwd / "units"
        print(app_dir)
        # Define the container configuration
        container_config = {
            'name': container_name,
            'ports': {5000: 5000},
            'detach': True,
            'volumes': [str(app_dir) + ':/app/app/', str(fluid_dir) + ':/app/fluids/', str(unit_dir) + ':/app/units/']
        }

        # Run the Docker container
        container = client.containers.run(image_name, **container_config)

        return container
    except docker.errors.APIError as e:
        # Handle Docker API errors here if needed
        print(f"Error: {str(e)}")
        return None

def stop_and_remove_containers(client,image_name):
    try:
        # List all containers with the specified image name
        containers = client.containers.list(filters={"ancestor": image_name})

        if not containers:
            print(f"No containers found for image '{image_name}'.")
            return

        # Stop each container
        for container in containers:
            container.stop()
            print(f"Container '{container.name}' has been stopped and removed.")
        
        # Prune containers
        deletedContainers = client.containers.prune()
        print("Deleted Containers:",deletedContainers)

        """
        # Stop and remove each container
        for container in containers:
            container.stop()
            container.remove()
            print(f"Container '{container.name}' has been stopped and removed.")
        """

    except docker.errors.APIError as e:
        # Handle Docker API errors here if needed
        print(f"Error: {str(e)}")

def remove_docker_image(client,image_name):
    try:
        # Remove the Docker image
        client.images.remove(image_name)

        print(f"Docker image '{image_name}' has been removed.")
    except docker.errors.ImageNotFound:
        print(f"Docker image '{image_name}' not found.")
    except docker.errors.APIError as e:
        # Handle Docker API errors here if needed
        print(f"Error: {str(e)}")

def run_sift_docker(client):
    image_name = 'sift'
    container_name = 'sift_container'
    port_mapping = {'5000': '5000'}

    container = run_docker_container(client,image_name, container_name, port_mapping)
    if container:
        print(f"Container '{container_name}' is running.")

if __name__ == "__main__":
    try:
        # Create a Docker client
        client = docker.from_env()
    except docker.errors.APIError as e:
        # Handle Docker API errors here if needed
        print(f"Error: {str(e)}")

    image_name = 'sift'
    container_name = 'sift_container'
    container = None
    if check_docker_image_exists(client,image_name):
        print(f"The Docker image '{image_name}' exists on the computer.")
        container = run_sift_docker(client)
    else:
        print(f"The Docker image '{image_name}' does not exist on the computer.")
        image = build_docker_image(client,image_name)
    
        if image:
            print(f"Docker image '{image_name}' has been built successfully.")
            container = run_sift_docker(client)
        else:
            print(f"Docker image '{image_name}' has NOT been built successfully.")
    
    print(client.containers.list())
    user_input = input("Press Enter to close container, type 'y' to delete image as well: ")

    my_thread = threading.Thread(target=stop_and_remove_containers(client,image_name))
    my_thread.start()
    my_thread.join()

    if user_input == 'y':
        remove_docker_image(client,image_name)
