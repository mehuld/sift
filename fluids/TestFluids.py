#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestFluids.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

# setting path to root dir
import sys
import math
import os
pathToRoot = os.environ.get("rootDir")
if pathToRoot:
    sys.path.append(pathToRoot)
else:
    sys.path.append('../')


from io import StringIO
from unittest import main, TestCase

from fluids_solver import colebrook_friction_factor, reynolds_number, incompressible_pipe_friction_loss, \
    forced_convec_htc_cylin, cylin_surface_area, compressible_CdA, compressible_flowrate, incompressible_CdA, \
    incompressible_flowrate, compressible_throat_orifice_velocity, joule_thomson

# -----------
# TestFluids
# -----------


class TestFluids (TestCase):

    def setUp(self):
        self.error_range = 0.1
        self.error_range_small=0.05

    # ----
    # colebrook friction factor
    # Input is reynolds number (dimensionless), diameter in m, and roughness in m
    # Using https://www.ajdesigner.com/php_colebrook/colebrook_equation.php#ajscroll to check
    # ----

    def test_colebrook_1(self):
        friction_factor = colebrook_friction_factor(10000, 0.021844, 0.0005)
        self.assertAlmostEqual(friction_factor, 0.05464, None, None, self.error_range*friction_factor)

    def test_colebrook_2(self):
        friction_factor = colebrook_friction_factor(100000, 0.01, 0.00001)
        self.assertAlmostEqual(friction_factor, 0.02217, None, None, self.error_range*friction_factor)

    def test_colebrook_3(self):
        friction_factor = colebrook_friction_factor(500, 0.04, 0.00001)
        self.assertAlmostEqual(friction_factor, 0.08137, None, None, self.error_range*friction_factor)

    # ----
    # reynolds
    # Input is pressure (Pa), temperature (K), velocity (m/s), diameter (m), fluid name as a string
    # Using https://www.omnicalculator.com/physics/reynolds-number to check
    # ----

    def test_reynolds_1(self):
        reynolds = reynolds_number(1000000, 300, 100, 0.01, 'Helium')
        self.assertAlmostEqual(reynolds, 80012, None, None, self.error_range*reynolds)

    def test_reynolds_2(self):
        reynolds = reynolds_number(30000, 350, 10, 0.1, 'Water')
        self.assertAlmostEqual(reynolds, 16326, None, None, self.error_range*reynolds)

    def test_reynolds_3(self):
        reynolds = reynolds_number(999999, 70, 20, 0.01, 'Oxygen')
        self.assertAlmostEqual(reynolds, 660548, None, None, self.error_range*reynolds)

    # ----
    # darcy weisbach incompressible flow pipe solver
    # Input is input and output pressure (psi), temperature (kelvin), diameter (in), length (m), roughness (m), fluid as a string
    # Using https://www.omnicalculator.com/physics/darcy-weisbach to check
    # ----

    def test_incompressible_friction_1(self):
        friction_loss = incompressible_pipe_friction_loss(159.037738, 14, 300, 0.4, 10, 0.00005, 'Water')
        self.assertAlmostEqual(friction_loss, 0.6626984082813686, None, None, self.error_range*friction_loss)

    def test_incompressible_friction_2(self):
        friction_loss = incompressible_pipe_friction_loss(159.037738, 14, 300, 0.4, 10, 0.00005, 'Helium')
        self.assertAlmostEqual(friction_loss, -1, None, None, self.error_range*friction_loss)

    def test_incompressible_friction_3(self):
        friction_loss = incompressible_pipe_friction_loss(1014, 14, 70, 0.86, 15, 0.00005, 'Oxygen') #psi and inches
        self.assertAlmostEqual(friction_loss, 11.998829304004609, None, None, self.error_range*friction_loss)

    # ----
    # heat transfer coefficient of forced convection around a cylinder solver using Churchil Bernstein correlation
    # Input is surr pressure, surface temp, surr temp, velocity, diameter and surr fluid
    # Using https://www.fxsolver.com/browse/formulas/Churchill%E2%80%93Bernstein+Equation to check
    # ----

    def test_forced_convec_htc_cylin_1(self):
        htc = forced_convec_htc_cylin(101325, 200, 300, 50, 0.381, 'AIR') #0.381m is trel copv diameter
        self.assertAlmostEqual(htc, 119, None, None, self.error_range*htc)

    def test_forced_convec_htc_cylin_2(self):
        htc = forced_convec_htc_cylin(101325, 300, 300, 0.01, 0.0001, 'AIR')
        self.assertAlmostEqual(htc, -1, None, None, self.error_range*htc)

    # ----
    # Input is diameter and height. Solves for cylinder surface area
    # Using https://www.omnicalculator.com/math/surface-area-of-cylinder to check
    # ----

    def test_cylin_surface_area_1(self):
        area = cylin_surface_area(0.1, 1)
        self.assertAlmostEqual(area, 0.32987, None, None, self.error_range*area)

    # ----
    # Input is flowrate, upstream and down pressure, upstream temp, fluid. Solves for CdA
    # Using https://utexas.sharepoint.com/:x:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7BF802287F-36EC-4C63-AFB5-CD0E5410C554%7D&file=Compressible%20Orifice.xlsm&action=default&mobileredirect=true and valcor to check
    # ----

    def test_compressible_CdA_1(self): #unchoked flow
        CdA = compressible_CdA(0.048080791, 1013250, 801325, 300, 'Helium')
        self.assertAlmostEqual(CdA, 6.4516e-5, None, None, self.error_range*CdA)

    def test_compressible_CdA_2(self): #choked flow
        CdA = compressible_CdA(0.0600102706, 1013250, 275790, 300, 'Helium')
        self.assertAlmostEqual(CdA, 6.4516e-5, None, None, self.error_range*CdA)

    def test_compressible_CdA_3(self): #choked flow, nitrogen
        CdA = compressible_CdA(1.19235826, 6.895e+6, 275790, 250, 'Nitrogen')
        self.assertAlmostEqual(CdA, 6.4516e-5, None, None, self.error_range*CdA)

    # ----
    # Input is CdA, upstream and down pressure, upstream temp, fluid. Solves for flowrate
    # Using https://utexas.sharepoint.com/:x:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7BF802287F-36EC-4C63-AFB5-CD0E5410C554%7D&file=Compressible%20Orifice.xlsm&action=default&mobileredirect=true and valcor to check
    # ----
        
    def test_compressible_flowrate_1(self): #unchoked flow
        flowrate = compressible_flowrate(6.4516e-5, 1013250, 801325, 300, 'Helium')
        self.assertAlmostEqual(flowrate, 0.048080791, None, None, self.error_range*flowrate)

    def test_compressible_flowrate_2(self): #choked flow
        flowrate = compressible_flowrate(6.4516e-5, 1013250, 275790, 300, 'Helium')
        self.assertAlmostEqual(flowrate, .0600102706, None, None, self.error_range*flowrate)

    def test_compressible_flowrate_3(self): #choked flow, nitrogen
        flowrate = compressible_flowrate(6.4516e-5, 6.895e+6, 275790, 250, 'Nitrogen')
        self.assertAlmostEqual(flowrate, 1.19235826 , None, None, self.error_range*flowrate)

    # ----
    # Input is flowrate, upstream and down pressure, temp, fluid. Solves for CdA
    # Using https://utexas.sharepoint.com/:x:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7B8F58C7E5-1E2D-44AD-B9B0-4AF0DA7A8BBD%7D&file=Incompressible%20Flow.xlsm&action=default&mobileredirect=true and valcor to check
    # ----

    def test_incompressible_CdA_1(self): #Water
        CdA = incompressible_CdA(1.8, 6894760, 100000, 300, 'Water')
        self.assertAlmostEqual(CdA, 0.0000155, None, None, self.error_range*CdA)

    def test_incompressible_CdA_2(self): #test cryogenic fluid
        CdA = incompressible_CdA(2, 689476, 100000, 70, 'Oxygen')
        self.assertAlmostEqual(CdA, 5.24e-05, None, None, self.error_range*CdA)

    def test_incompressible_CdA_3(self): #return -1 at gas input
        CdA = incompressible_CdA(1, 700000, 100000, 500, 'Water')
        self.assertAlmostEqual(CdA, -1, None, None, self.error_range*CdA)

    # ----
    # Input is CdA, upstream and down pressure, temp, fluid. Solves for flowrate
    # Using https://utexas.sharepoint.com/:x:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7B8F58C7E5-1E2D-44AD-B9B0-4AF0DA7A8BBD%7D&file=Incompressible%20Flow.xlsm&action=default&mobileredirect=true and valcor to check
    # ----

    def test_incompressible_flowrate_1(self): #Water
        flowrate = incompressible_flowrate(1.55e-5, 6894760, 100000, 300, 'Water')
        self.assertAlmostEqual(flowrate, 1.8, None, None, self.error_range*flowrate)

    def test_incompressible_flowrate_2(self): #test cryogenic fluid
        flowrate = incompressible_flowrate(5.24e-05, 689476, 100000, 70, 'Oxygen')
        self.assertAlmostEqual(flowrate, 2, None, None, self.error_range*flowrate)

    def test_incompressible_flowrate_3(self): #return -1 at gas input
        flowrate = incompressible_flowrate(0.00001, 700000, 100000, 500, 'Water')
        self.assertAlmostEqual(flowrate, -1, None, None, self.error_range*flowrate)


    # ----
    # Input is upstream pressure and temperature along with the fluid name. Solves for downstream temperature
    # Derived dT using https://calculator.academy/joule-thomson-effect-calculator/
    # Joule-Thomson (JT) Coefficient from https://www.sciencedirect.com/topics/chemistry/joule-thomson-coefficient & https://www.sciencedirect.com/science/article/pii/S0955598605000579
    # ----

    def test_joule_thomson_1(self):  # Gaseous Water (steam)
        temperature, _ = joule_thomson(1.2e5, 379.15, .9e5, 'Water') # JT coef of − 5.8223 K/Bar
        self.assertAlmostEqual(temperature,  377.40, None, None, self.error_range_small * temperature)
    def test_joule_thomson_2(self):  #  Supercritical region
        temperature, _ = joule_thomson(401.9e5, 650.95, 250e5, 'Water') # JT coef of − 0.0526 K/Bar
        self.assertAlmostEqual(temperature, 642.96, None, None, self.error_range_small* temperature)
    def test_joule_thomson_3(self): # Methane
        temperature, _ = joule_thomson(5e6, 245, 3e6, 'Methane') # JT coef of − 0.6 K/Bar
        self.assertAlmostEqual(temperature, 233, None, None, self.error_range_small* temperature)
    def test_joule_thomson_4(self): # Methane
        temperature, _ = joule_thomson(4e6, 325, 3e6, 'Methane') # JT coef of − 2 K/Bar
        self.assertAlmostEqual(temperature, 335, None, None, self.error_range_small* temperature)
    


'''
    # ----
    # Input is flowrate, CdA, throat_density, throat_gamma and throat speed of sound. Solves for throat velocity
    # Using Valcor to check barely choked properties to verify against that fluid's speed of sound
    # ----

    def test_compressible_throat_orifice_velocity_1(self): #Test barely choked Helium
        throat_velocity = compressible_throat_orifice_velocity(0, 0, 0, 0, 0)
        self.assertAlmostEqual(throat_velocity, 0, None, None, self.error_range*throat_velocity)

    def test_compressible_throat_orifice_velocity_2(self): #Test barely choked Nitrogen
        throat_velocity = compressible_throat_orifice_velocity(0, 0, 0, 0, 0)
        self.assertAlmostEqual(throat_velocity, 0, None, None, self.error_range*throat_velocity)
'''
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestFluids.py >  TestFluids.out 2>&1


$ cat TestFluids.out
.......
----------------------------------------------------------------------
Ran 21 tests in 0.043s
OK


$ coverage report -m                   >> TestFluids.out



$ cat TestFluids.out
.......
----------------------------------------------------------------------
Ran 21 tests in 0.043s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          33      2     16      2    92%   36, 39
TestFluids.py      94      0      0      0   100%
------------------------------------------------------------
TOTAL              127      2     16      2    97%
"""
