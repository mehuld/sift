import sys
 
# setting path
sys.path.append('../sift')

from CoolProp.CoolProp import PropsSI
from CoolProp.CoolProp import PhaseSI
from CoolProp.CoolProp import AbstractState
import numpy as np
import math
import matplotlib.pyplot as plt
from io import BytesIO
from units.units import Pa2psi, K2R, K2F, K2C, Pa2Bar, Pa2MPa

'''
Input fluid name to generate a phase diagram.
'''
def plot_phase_diagram(fluid_name, pressure_unit, temperature_unit, num_points=500):

    AS = AbstractState("HEOS", fluid_name)
    T_triple = PropsSI('Ttriple', fluid_name)
    P_triple = PropsSI('P', 'T', T_triple, 'Q', 0, fluid_name)
    T_critical = PropsSI('Tcrit', fluid_name)
    P_critical = PropsSI('Pcrit', fluid_name)

    temp_min = T_triple
    temp_max = T_critical
    pressure_min = P_triple
    pressure_max = P_critical * 50

    # Create temperature range for plotting saturation curve
    T_sat = np.linspace(temp_min, temp_max, num_points)
    P_sat = np.array([PropsSI('P', 'T', T, 'Q', 1, fluid_name) for T in T_sat])

    T_melt_points = []
    P_melt_points = []
    P_range_melting = np.logspace(np.log10(pressure_min), np.log10(pressure_max), num_points)
    for P in P_range_melting:
        try:
            T_melt = AS.melting_line(0, 1, P)
            T_melt_points.append(T_melt)
            P_melt_points.append(P)
        except:
            continue

    P_melt_points = [convert_units(p, pressure_unit) for p in P_melt_points]
    P_sat = [convert_units(p, pressure_unit) for p in P_sat]
    P_triple = convert_units(P_triple, pressure_unit)
    P_critical = convert_units(P_critical, pressure_unit)
    T_melt_points = [convert_units(T, temperature_unit) for T in T_melt_points]
    T_sat = [convert_units(T, temperature_unit) for T in T_sat]
    T_triple = convert_units(T_triple, temperature_unit)
    T_critical = convert_units(T_critical, temperature_unit)

    fig, ax = plt.subplots(figsize=(10, 6))
    ax.plot(T_melt_points, P_melt_points, 'r-', label='Solid-Liquid (Melting Curve)')
    ax.plot(T_sat, P_sat, 'b-', label='Liquid-Gas (Saturation Curve)')
    ax.plot(T_triple, P_triple, 'go', label='Triple Point')
    ax.plot(T_critical, P_critical, 'ro', label='Critical Point')

    #Fill
    P_common = np.interp(T_sat, T_melt_points, P_melt_points)
    ax.fill_between(T_sat, P_common, P_sat, color='lightblue', alpha=0.5, label='Liquid')

    ax.set_xlabel(f'Temperature ({temperature_unit})')
    ax.set_ylabel(f'Pressure ({pressure_unit})')
    ax.set_yscale('log')
    ax.set_title(f'Phase Diagram for {fluid_name}')
    ax.legend()
    ax.grid(True)

    # Save the plot to a BytesIO object and return it - for the front end
    img = BytesIO()
    fig.savefig(img, format='png', bbox_inches='tight', pad_inches=0.1)
    img.seek(0)
    plt.close(fig)
    return img

def convert_units(value, to_unit):
    conversion_factors = {
        'psi': Pa2psi,  # Pascal to PSI
        'MPa': Pa2MPa, # Pascal to MPa
        'Bar': Pa2Bar, # Pascals to Bar
        'R': K2R, # Kelvin to Rankine
        'F': K2F, # Kelvin to Farenheight
        'C': K2C # Kelvin to Celsius
    }
    convert = conversion_factors.get(to_unit, lambda x: x)
    return convert(value)