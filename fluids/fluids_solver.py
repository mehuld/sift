import sys
 
# setting path
sys.path.append('../sift')

from CoolProp.CoolProp import PropsSI
from CoolProp.CoolProp import PhaseSI
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
    
'''
Input is reynolds number (dimensionless), diameter in m, and roughness in m
Output is the friction_factor implicitly solved using the colebrook implicit equation
'''
def colebrook_friction_factor(reynolds, diameter, roughness):
    x = lambda f: (2 * math.log10((roughness / (3.7 * diameter)) + (2.51 / (reynolds * math.sqrt(f))))) + (1 / math.sqrt(f))
    friction_factor = fsolve(x, 0.001)[0]
    return friction_factor
'''
Input is pressure (Pa), temperature (K), velocity (m/s), diameter (m), fluid name as a string
Output is the reynolds number solved using rho * v * d / mu
'''
def reynolds_number(pressure, temperature, velocity, diameter, fluid):
    density = PropsSI('D','P',pressure,'T',temperature,fluid) #kg/m^3
    viscosity = PropsSI('V','P',pressure,'T',temperature,fluid) #kg/m^3
    return (density * velocity * diameter) / viscosity

'''
Input is input and output pressure (psi), temperature (kelvin), diameter (in), length (m), roughness (m), fluid as a string and a guess for flowrate
Output is the flowrate in kg/s. Solved by iterating through darcy weisbach
'''
def incompressible_pipe_friction_loss(input_pressure, output_pressure, temperature, diameter, length, roughness, fluid, flow_rate_guess=1):
    
    delta_pressure = (input_pressure - output_pressure)* 6894.76 #psi to pascals, pressure drop
    av_pressure = ((input_pressure + output_pressure)/2)* 6894.76 #psi to pascals, averaging pressure across drop

    if PhaseSI('P',av_pressure,'T',temperature, fluid) != 'liquid':
        return -1

    area = math.pi * (((diameter/2) * 0.0254)**2)
    diameter = diameter * 0.0254 #inches to m
    density = PropsSI('D','P',av_pressure,'T',temperature,fluid) #kg/m^3
    #Density is slightly incorrect since delta_pressure not pressure is used but since this is for incompressible fluids this should have negligible effect
    velocity = flow_rate_guess / (density * area)
    fun = lambda velocity: (colebrook_friction_factor(reynolds_number(av_pressure,temperature,velocity,diameter, fluid),diameter, roughness) 
                                            * 0.5 * (density) * (velocity**2) * (length / diameter)) - delta_pressure

    velocity_solution = fsolve(fun, 100)[0]
    return velocity_solution * density * area

'''
solves for the heat transfer coefficient of forced convection around a cylinder
Uses the forced convection cylinder Churchill Bernstein correlation for Nusselts number
surr = surrounding, temp = temperature, cylin = cylinder, htc = heat transfer coefficient 
the characteristic length for a cylinder is the external diameter. Fluid properties are evaluated at film temp
Valid as long as Reynolds*Prandtl number is > 0.2
Input is surr pressure, surface temp, surr temp, velocity, diameter and surr fluid
Ouptut is the heat transfer coefficient
'''
def forced_convec_htc_cylin(surr_pressure, surface_temp, surr_temp, velocity, diameter, surr_fluid):
    film_temp = (surr_temp + surface_temp) / 2
    Re = reynolds_number(surr_pressure, film_temp, velocity, diameter, surr_fluid)
    Pr = PropsSI('PRANDTL', 'P', surr_pressure, 'T', film_temp, surr_fluid)

    if Re*Pr < 0.2:
        return -1

    Nu = 0.3 + (((0.62 * (Re**0.5) * (Pr**(1/3))) /
                 ((1 + ((0.4 * Pr) ** (2/3))) ** (1/4))
                 ) * ((1 + ((Re/282000) ** (5/8))) ** (4/5)))
    k = PropsSI('L', 'P', surr_pressure, 'T', film_temp, surr_fluid)
    return (k * Nu) / diameter

'''
Input is diameter (m), height (m)
Output is surface area (m^2)
'''
def cylin_surface_area(diameter, height):
    radius = diameter/2
    return ((2 * math.pi * height * radius) + (2 * math.pi * (radius ** 2)))

'''
Input is input and output pressure (Pa), temperature (K), sum_of_K, fluid name and a guess for velocity (optional)
Output is the flowrate in kg/s. Solved by iterating through K resistance equation from Crane
IN DEVELOPMENT
'''
def incompressible_velocity(input_pressure, output_pressure, temperature, sum_of_K, fluid, velocity_guess=1):
    delta_pressure = (input_pressure - output_pressure)* 6894.76 #psi to pascals, pressure drop
    av_pressure = ((input_pressure + output_pressure)/2)* 6894.76 #psi to pascals, averaging pressure across drop

    if PhaseSI('P',av_pressure,'T',temperature, fluid) != 'liquid':
        return -1
    
    density = PropsSI('D','P',av_pressure,'T',temperature,fluid) #kg/m^3
    #Density is slightly incorrect since delta_pressure not pressure is used but since this is for incompressible fluids this should have negligible effect
    fun = lambda velocity_guess: (0.5 * density * (velocity_guess ** 2) * sum_of_K) - delta_pressure
    velocity_solution = fsolve(fun, 100)[0]
    return velocity_solution 

'''
Input is flowrate (kg/s), input pressure (Pa), output pressure (Pa), temperature (K), fluid
Output is the CdA
'''
def compressible_CdA(flowrate, up_pressure, down_pressure, up_temp, fluid):
    density = PropsSI('D','P',up_pressure,'T',up_temp,fluid) #kg/m^3
    gamma = PropsSI('ISENTROPIC_EXPANSION_COEFFICIENT','P',up_pressure,'T',up_temp,fluid)
    gas_constant = PropsSI('GAS_CONSTANT','P',up_pressure,'T',up_temp,fluid) #J/Kmols
    R = (gas_constant / PropsSI('MOLARMASS','P',up_pressure,'T',up_temp,fluid)) #J/kgK
    gc = 1

    p_crit = up_pressure * ((2 / (gamma + 1)) ** (gamma / (gamma - 1))) #critical pressure ratio is a function of stagnation pressure being used analagous to static here, https://en.wikipedia.org/wiki/Choked_flow
    if down_pressure > p_crit:
        CdA = flowrate / (
            density *
            ((down_pressure/up_pressure)**(1/gamma)) *
            math.sqrt(
                (2 * gc * R * up_temp * (gamma / (gamma-1))) *
                (1 - ((down_pressure/up_pressure)**((gamma - 1)/gamma)))
            )
        )
        return CdA
    else:
        CdA = flowrate / (
            up_pressure *
            math.sqrt(
                ((gc * gamma)/(R * up_temp)) * 
                ((2 / (gamma + 1)) ** ((gamma+1)/(gamma-1)))
            )
        )
        return CdA
    
'''
Input is CdA (unitless), input pressure (Pa), output pressure (Pa), temperature (K), fluid
Output is the flowrate (kg/s)
'''
def compressible_flowrate(CdA, up_pressure, down_pressure, up_temp, fluid):
    density = PropsSI('D','P',up_pressure,'T',up_temp,fluid) #kg/m^3
    gamma = PropsSI('ISENTROPIC_EXPANSION_COEFFICIENT','P',up_pressure,'T',up_temp,fluid)
    gas_constant = PropsSI('GAS_CONSTANT','P',up_pressure,'T',up_temp,fluid) #J/Kmols
    R = (gas_constant / PropsSI('MOLARMASS','P',up_pressure,'T',up_temp,fluid)) #J/kgK
    gc = 1

    p_crit = up_pressure * ((2 / (gamma + 1)) ** (gamma / (gamma - 1))) #critical pressure ratio is a function of stagnation pressure being used analagous to static here, https://en.wikipedia.org/wiki/Choked_flow
    if down_pressure > p_crit:
        flowrate = CdA * (
            density *
            ((down_pressure/up_pressure)**(1/gamma)) *
            math.sqrt(
                (2 * gc * R * up_temp * (gamma / (gamma-1))) *
                (1 - ((down_pressure/up_pressure)**((gamma - 1)/gamma)))
            )
        )
        return flowrate
    else:
        flowrate = CdA * (
            up_pressure *
            math.sqrt(
                ((gc * gamma)/(R * up_temp)) * 
                ((2 / (gamma + 1)) ** ((gamma+1)/(gamma-1)))
            )
        )
        return flowrate
    
'''
Input is flowrate (kg/s), input pressure (Pa), output pressure (Pa), fluid
Output is the CdA
Checks throat pressure to check if cavitating. If so, returns -1
IN DEVELOPMENT
'''
def incompressible_CdA(flowrate, up_pressure, down_pressure, up_temp, fluid):
    density = PropsSI('D','P',up_pressure,'T',up_temp,fluid) #kg/m^3

    if PhaseSI('P',up_pressure,'T',up_temp, fluid) != 'liquid' or \
        PhaseSI('P',down_pressure,'T',up_temp, fluid) != 'liquid':
        return -1

    CdA = (flowrate/density) / math.sqrt(2 * ((up_pressure - down_pressure) / density))
    return CdA

'''
Input is CdA (m^2), input pressure (Pa), output pressure (Pa), fluid
Output is the flowrate (kg/s)
Checks throat pressure to check if cavitating. If so, returns -1
IN DEVELOPMENT
'''
def incompressible_flowrate(CdA, up_pressure, down_pressure, up_temp, fluid):
    density = PropsSI('D','P',up_pressure,'T',up_temp,fluid) #kg/m^3

    if PhaseSI('P',up_pressure,'T',up_temp, fluid) != 'liquid' or \
        PhaseSI('P',down_pressure,'T',up_temp, fluid) != 'liquid':
        return -1
    
    flowrate = density * (CdA * math.sqrt(2 * ((up_pressure - down_pressure) / density)))
    return flowrate

'''
Input is flowrate (kg/s), CdA (unitless), throat_density (kg/m^3), throat_gamma (unitless) and throat speed of sound
Output is the throat velocity in m/s. Solved by iterating through isentropic and continuity equations
IN DEVELOPMENT
'''
def compressible_throat_orifice_velocity(flowrate, CdA, throat_density, throat_gamma, throat_speed_of_sound):

    fun = lambda velocity: flowrate - (velocity * CdA * throat_density * ((1 + (((throat_gamma - 1) / 2) * \
                            (velocity / throat_speed_of_sound))) ** ((-1)/(throat_gamma - 1))))
    velocity_solution = fsolve(fun, 0)[0]
    return velocity_solution

'''
Input is upstream pressure (Pa), upstream temperature (K), and the fluid name
Output is downstream pressure (Pa). Solved by iterating pulling joule-thomson coeffient from CoolProp and deriving temperature.
IN DEVELOPMENT
'''
def joule_thomson(upstream_pressure,upstream_temperature,downstream_pressure,fluid_name):
    joule_thomson_coef = PropsSI('d(T)/d(P)|H', 'T', upstream_temperature, 'P', upstream_pressure, fluid_name)
    joule_thomson_solution = upstream_temperature - joule_thomson_coef * (upstream_pressure-downstream_pressure)
    return (joule_thomson_solution, joule_thomson_coef)