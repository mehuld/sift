#Build from python image
FROM python:3.8-slim

### Useful docker bash commands
#$ docker r mi -f $(docker images -q)

#Test this to see if it would fix root issues
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

#Install dependencies
RUN pip install --trusted-host pypi.python.org -r requirements.txt

#Expose port 5000 on device
EXPOSE 5000

#Set Path to Root Dir
ENV rootDir=../../app

#Run web app
CMD ["python","app/app.py"] 