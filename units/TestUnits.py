#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from units import in2m, m2ft, kg2lb, K2R, Pa2lbft2, ft22m2, kgm32lbft3, JkgK2ftlbR, Pa2psi, psi2Pa \
, MPa2Pa, R2K, F2K, C2K, Bar2Pa, m2in, m22in2, lb2kg, in22m2, lbft32kgm3, kJkg2Jkg, BTUlb2Jkg, calg2Jkg \
, BTUlbR2JkgK, BTUhrftF2WmK, cP2Nsm2, lbsft22Nsm2, cm22m2, K2F, K2C, Pa2MPa, Pa2Bar

# -----------
# TestUnits
# -----------


class TestFluids (TestCase):

    def setUp(self):
        self.error_range = 0.1

    # Input is inches, output is meters

    def test_in2m_1(self):
        meters = in2m(1)
        self.assertAlmostEqual(meters, 0.0254, None, None, self.error_range*meters)

    def test_in2m_2(self):
        meters = in2m(0.05)
        self.assertAlmostEqual(meters, 0.00127, None, None, self.error_range*meters) 

    # Input is meters, output is feet

    def test_m2f_1(self):
        feet = m2ft(1)
        self.assertAlmostEqual(feet, 3.28084, None, None, self.error_range*feet)

    def test_m2f_2(self):
        feet = m2ft(0.05)
        self.assertAlmostEqual(feet, 0.164042, None, None, self.error_range*feet)

    # Input is kg, output is lbs

    def test_kg2lb_1(self):
        lbs = kg2lb(1)
        self.assertAlmostEqual(lbs, 2.20462, None, None, self.error_range*lbs)

    def test_kg2lb_2(self):
        lbs = kg2lb(0.05)
        self.assertAlmostEqual(lbs, 0.1102311, None, None, self.error_range*lbs)

    # Input is kelvin, output is R

    def test_K2R_1(self):
        rankine = K2R(1)
        self.assertAlmostEqual(rankine, 1.8, None, None, self.error_range*rankine)

    def test_K2R_2(self):
        rankine = K2R(300)
        self.assertAlmostEqual(rankine, 540, None, None, self.error_range*rankine)

    # Input is Pascals, output is lb/ft^2
    
    def test_Pa2lbft2_1(self):
        lbft2 = Pa2lbft2(1)
        self.assertAlmostEqual(lbft2, 0.0208854342, None, None, self.error_range*lbft2)

    def test_Pa2lbft2_2(self):
        lbft2 = Pa2lbft2(101325)
        self.assertAlmostEqual(lbft2, 2116.2166237, None, None, self.error_range*lbft2)

    # Input is feet, output is meters

    def test_ft2m_1(self):
        meters = ft22m2(1)
        self.assertAlmostEqual(meters, 0.092903, None, None, self.error_range*meters)
         
    def test_ft2m_2(self):
        meters = ft22m2(0.05)
        self.assertAlmostEqual(meters, 0.004645152, None, None, self.error_range*meters)

    # Input kg/m^3, output is lb/ft^3

    def test_kgm32lbft3_1(self):
        lbft3 = kgm32lbft3(1)
        self.assertAlmostEqual(lbft3, 0.062428, None, None, self.error_range*lbft3)

    def test_kgm32lbft3_2(self):
        lbft3 = kgm32lbft3(0.05)
        self.assertAlmostEqual(lbft3, 0.003121398, None, None, self.error_range*lbft3)

    # Input J/kgK, output is ftlb/lbm R

    def test_JkgK2ftlbR_1(self):
        ftlbR = JkgK2ftlbR(1)
        self.assertAlmostEqual(ftlbR, 0.1859, None, None, self.error_range*ftlbR)

    def test_JkgK2ftlbR_2(self):
        ftlbR = JkgK2ftlbR(2077.1)
        self.assertAlmostEqual(ftlbR, 386.0551, None, None, self.error_range*ftlbR)

    # Input Pa, output is psi
        
    def test_Pa2psi_1(self):
        psi = Pa2psi(101325)
        self.assertAlmostEqual(psi, 14.7, None, None, self.error_range*psi)

    def test_Pa2psi_2(self):
        psi = Pa2psi(6.895e+6)
        self.assertAlmostEqual(psi, 1000, None, None, self.error_range*psi)

    # Input psi, output is Pa
        
    def test_psi2Pa_1(self):
        Pa = psi2Pa(14.7)
        self.assertAlmostEqual(Pa, 101325, None, None, self.error_range*Pa)

    def test_psi2Pa_2(self):
        Pa = psi2Pa(1000)
        self.assertAlmostEqual(Pa, 6.895e+6, None, None, self.error_range*Pa)

    # Input MPa, output is Pa
        
    def test_MPa2Pa_1(self):
        Pa = MPa2Pa(1)
        self.assertAlmostEqual(Pa, 1000000, None, None, self.error_range*Pa)

    # Input Bar, output is Pa
        
    def test_Bar2Pa_1(self):
        Pa = Bar2Pa(1)
        self.assertAlmostEqual(Pa, 100000, None, None, self.error_range*Pa)

    # Input R, output is K
        
    def test_R2K_1(self):
        K = R2K(500)
        self.assertAlmostEqual(K, 277.778, None, None, self.error_range*K)
    
    # Input F, output is K
        
    def test_F2K_1(self):
        K = F2K(70)
        self.assertAlmostEqual(K, 294.261, None, None, self.error_range*K)

    # Input C, output is K
    
    def test_C2K_1(self):
        K = C2K(20)
        self.assertAlmostEqual(K, 293.15, None, None, self.error_range*K)

    # Input meters, output is in
        
    def test_m2in_1(self):
        inches = m2in(1)
        self.assertAlmostEqual(inches, 39.3701, None, None, self.error_range * inches)

    # Input meters squared, output is inches squared
        
    def test_m22in2_1(self):
        inchessquared = m22in2(1)
        self.assertAlmostEqual(inchessquared, 1550, None, None, self.error_range * inchessquared)

    # Input inches squared, output is meters squared
        
    def test_in22m2_1(self):
        msquared = in22m2(100)
        self.assertAlmostEqual(msquared, 0.064516, None, None, self.error_range * msquared)

    # Input centimeters squared, output is meters squared
        
    def test_cm22m2_1(self):
        msquared = cm22m2(100)
        self.assertAlmostEqual(msquared, 0.01, None, None, self.error_range * msquared)

    # Input lb, output is kg
        
    def test_lb2kg_1(self):
        kg = lb2kg(1)
        self.assertAlmostEqual(kg, 0.453592, None, None, self.error_range * kg)

    # Input lb/ft^3, output is kg/m^3
    
    def test_lbft3_1(self):
        kgm3 = lbft32kgm3(1)
        self.assertAlmostEqual(kgm3, 16.0185, None, None, self.error_range * kgm3)

    # Input kJ/kg, output is J/kg
    def test_kJkg2Jkg_1(self):
        Jkg = kJkg2Jkg(1)
        self.assertAlmostEqual(Jkg, 1000, None, None, self.error_range * Jkg)

    #Input cal/g, output is J/kg
    def test_calg2Jkg_1(self):
        Jkg = calg2Jkg(1)
        self.assertAlmostEqual(Jkg, 4186.8, None, None, self.error_range * Jkg)

    #Input BTU/lb, output is J/kg
    def test_BTUlb2Jkg_1(self):
        Jkg = BTUlb2Jkg(1)
        self.assertAlmostEqual(Jkg, 2326, None, None, self.error_range * Jkg)

    #Input BTU/lbR, output is J/kgK
    def test_BTUlbR2JkgK_1(self):
        JkgK = BTUlbR2JkgK(1)
        self.assertAlmostEqual(JkgK, 4186.8, None, None, self.error_range * JkgK)

    #Input BTU/hrftF, output is W/mK (thermal conductivity)
    def test_BTUhrftF2WmK_1(self):
        WmK = BTUhrftF2WmK(1)
        self.assertAlmostEqual(WmK, 1.7296, None, None, self.error_range * WmK)

    #Input cP, output is Ns/m^2 (viscosity)
    def test_cP2Nsm2_1(self):
        Nsm2 = cP2Nsm2(1)
        self.assertAlmostEqual(Nsm2, 0.001, None, None, self.error_range * Nsm2)

    #Input lbs/ft^2, output is Ns/m^2 (viscosity)
    def test_lbsft22Nsm2_1(self):
        Nsm2 = lbsft22Nsm2(1)
        self.assertAlmostEqual(Nsm2, 47.880259, None, None, self.error_range * Nsm2)

    #Input K, output is F
    def test_K2F_1(self):
        F = K2F(400)
        self.assertAlmostEqual(F, 260.33, None, None, self.error_range * F)

    #Input K, output is C
    def test_K2C_1(self):
        C = K2C(400)
        self.assertAlmostEqual(C, 126.85, None, None, self.error_range * C)

    # Input Pa, output is MPa
    def test_Pa2MPa_1(self):
        MPa = Pa2MPa(1000000)
        self.assertAlmostEqual(MPa, 1, None, None, self.error_range*MPa)

    # Input Pa, output is Bar
    def test_Pa2Bar_1(self):
        Bar = Pa2Bar(100000)
        self.assertAlmostEqual(Bar, 1, None, None, self.error_range*Bar)

if __name__ == "__main__": 
    main()

""" #pragma: no cover
$ coverage run --branch TestUnits.py >  TestUnits.out 2>&1
"""