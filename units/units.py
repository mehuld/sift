#input inches, return meters
def in2m(inches):
    return inches * 0.0254

#input meters, return feet
def m2ft(meters):
    return meters * 3.28084

#input kg, return lbs
def kg2lb(kg):
    return kg * 2.20462

#input kelvin, return rankine
def K2R(kelvin):
    return kelvin * 1.8

#input Pascals, return lb/ft^2
def Pa2lbft2(Pa):
    return Pa * 0.0208854342

#input feet squared, return meters squared
def ft22m2(feet2):
    return feet2 * 0.092903

#input kg/m^3, return lb/ft^3
def kgm32lbft3(kgm3):
    return kgm3 * 0.062428

#input J/kgK, return ftlb/lbm R
def JkgK2ftlbR(JkgK):
    return JkgK * 0.1859

#input Pa, return psi
def Pa2psi(Pa):
    return Pa * 0.000145038

#input psi, return Pa
def psi2Pa(psi):
    return psi * 6894.76

#input MPa, return Pa
def MPa2Pa(MPa):
    return MPa * 1000000

def Bar2Pa(Bar):
    return Bar * 100000

#input Pa, return MPa
def Pa2MPa(Pa):
    return Pa / 1000000

def Pa2Bar(Pa):
    return Pa / 100000

#input R, return K
def R2K(R):
    return R * 0.555556

#input F, return K
def F2K(F):
    return (F + 459.67) * (5/9)

#input C, return K
def C2K(C):
    return C + 273.15

#input K, return F
def K2F(K):
    return ((K - 273.15) * 1.8) + 32

#input K, return C
def K2C(K):
    return K - 273.15

#input meters, return inches
def m2in(m):
    return m * 39.3701

#input meters squared, return inches squared
def m22in2(msquared):
    return msquared * 1550

#input inches squared, return meters squared
def in22m2(inchessquared):
    return inchessquared * 0.00064516

#input centimeters squared, return meters squared
def cm22m2(cmsquared):
    return cmsquared * 0.0001

#input lb/s, return kg/s
def lb2kg(lb):
    return lb * 0.453592

#Input lb/ft^3 to kg/m^3
def lbft32kgm3(lbft3):
    return lbft3 * 16.0185

#Input kJ/kg to J/kg
def kJkg2Jkg(kJkg):
    return kJkg * 1000

#Input cal/g to J/kg
def calg2Jkg(calg):
    return calg *  4186.8

#Input BTU/lb to J/kg
def BTUlb2Jkg(BTUlb):
    return BTUlb * 2326

#Input BTU/lbR to J/kgK
def BTUlbR2JkgK(BTUlbR):
    return BTUlbR * 4186.8

#Input BTU/hrftF to W/mK (thermal conductivity)
def BTUhrftF2WmK(BTUhrftF):
    return BTUhrftF * 1.7296

#Input cP to Ns/m^2 (viscosity)
def cP2Nsm2(cP):
    return cP * 0.001

#Input lbs/ft^2 to Ns/m^2 (viscosity)
def lbsft22Nsm2(lbsft2):
    return lbsft2 * 47.880259

# Input feet, return meters
def ft2m(feet):
    return feet / 3.28084

# Input lb/ft^2, return Pascals
def lbft22Pa(lbft2):
    return lbft2 / 0.0208854342

# Input meters squared, return feet squared
def m22ft2(meters2):
    return meters2 / 0.092903

# Input ftlb/lbm R, return J/kgK
def ftlbR2JkgK(ftlbR):
    return ftlbR / 0.1859

# Input meters squared, return centimeters squared
def m22cm2(meters2):
    return meters2 * 10000

# Input J/kg, return kJ/kg
def Jkg2kJkg(Jkg):
    return Jkg / 1000

# Input J/kg, return cal/g
def Jkg2calg(Jkg):
    return Jkg / 4186.8

# Input J/kg, return BTU/lb
def Jkg2BTUlb(Jkg):
    return Jkg / 2326

# Input J/kgK, return BTU/lbR
def JkgK2BTUlbR(JkgK):
    return JkgK / 4186.8

# Input W/mK (thermal conductivity), return BTU/hrftF
def WmK2BTUhrftF(WmK):
    return WmK / 1.7296

# Input Ns/m^2 (viscosity), return cP
def Nsm22cP(Nsm2):
    return Nsm2 / 0.001

# Input Ns/m^2 (viscosity), return lbs/ft^2
def Nsm22lbsft2(Nsm2):
    return Nsm2 / 47.880259