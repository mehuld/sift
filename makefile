.PHONY: Fluids.log

FILES :=                              \
    fluids_solver.html                      \
    fluids_solver.log                       \
    fluids/fluids_solver.py                        \
	units/units.py                       \
    fluids/TestFluids.out                   \
    fluids/TestFluids.py					  \
	units/TestUnits.py

ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/usr/cs330e -w /usr/cs330e fareszf/python
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
    DOC := docker run -it -v /$$(PWD):/usr/cs330e -w //usr/cs330e fareszf/python
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
    DOC := docker run -it -v $$(PWD):/usr/cs330e -w /usr/cs330e fareszf/python
endif

Fluids.html: fluids/fluids_solver.py
#	$(PYDOC) -w fluids/fluids_solver.py

Fluids.log:
	git log > fluids_solver.log

RunFluids.tmp: RunFluids.in RunFluids.out RunFluids.py
	$(PYTHON) RunFluids.py < RunFluids.in > RunFluids.tmp
	diff --strip-trailing-cr RunFluids.tmp RunFluids.out

TestFluids.tmp: fluids/TestFluids.py
	$(COVERAGE) run    --source=fluids --branch fluids/TestFluids.py >  fluids/TestFluids.tmp 2>&1
	$(COVERAGE) report -m                      >> fluids/TestFluids.tmp
	cat fluids/TestFluids.tmp

TestUnits.tmp: units/TestUnits.py
	$(COVERAGE) run    --branch units/TestUnits.py >  units/TestUnits.tmp 2>&1
	$(COVERAGE) report -m                      >> units/TestUnits.tmp
	cat units/TestUnits.tmp

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  TestUnits.tmp
	rm -f  TestFluids.tmp
	rm -rf __pycache__
	rm -rf cs330e-Fluids-tests
	
docker:
	$(DOC)
	
config:
	git config -l

format:
	$(AUTOPEP8) -i fluids_solver.py
	$(AUTOPEP8) -i RunFluids.py
	$(AUTOPEP8) -i TestFluids.py

scrub:
	make clean
	rm -f  fluids_solver.html
	rm -f  fluids_solver.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status
	
versions:
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version

test: Fluids.html Fluids.log TestUnits.tmp TestFluids.tmp check

docker_remove_image: 
	docker rmi -f sift

docker_image:
	docker build -t sift .

docker_run: 
	docker run -v "/${PWD}/app":/app/app/ -p 5000:5000 sift

docker_terminal_windows:
	docker run -v "/${PWD}/app":/app/app/ -p 5000:5000 -d --name sift sift
	winpty docker exec -it sift ../bin/bash


