# setting path
import sys
import os
import math
pathToRoot = os.environ.get("rootDir")
if pathToRoot:
    sys.path.append(pathToRoot)
else:
    sys.path.append('../')

from flask import Flask, render_template, request, jsonify, send_file
from fluids.fluids_solver import compressible_CdA, compressible_flowrate, incompressible_CdA, incompressible_flowrate, joule_thomson
from units.units import psi2Pa, MPa2Pa, Bar2Pa, R2K, C2K, F2K, m22in2, in2m, in22m2, cm22m2, lb2kg, lbft32kgm3, \
kJkg2Jkg, calg2Jkg, BTUlb2Jkg, BTUlbR2JkgK, BTUhrftF2WmK, cP2Nsm2, lbsft22Nsm2, K2F, K2C, K2R
from fluids.phase_diagram import plot_phase_diagram
from CoolProp.CoolProp import PropsSI
import matplotlib
matplotlib.use('Agg')  # Ensure matplotlib does not use any GUI backend
from io import BytesIO

app = Flask(__name__)

@app.route('/', methods=['GET'])
def serve_orifice_calculator_page():
    # Handle GET requests or render the template
    #return render_template('input_form.html')
    return render_template('HomePage.html')

@app.route('/incompressibleorifice')
def incompressibleorifice():
    return render_template('IncompressiblePage.html')

@app.route('/joulethomson')
def joulethomson():
    return render_template('JouleThomson.html')

@app.route('/compressibleorifice')
def compressibleorifice():
    return render_template('CompressiblePage.html')

@app.route('/propertylookup')
def propertylookup():
    return render_template('PropertyLookupPage.html')

@app.route('/phasediagram')
def phasediagram():
    return render_template('PhaseDiagramPage.html')

@app.route('/about')
def about():
    return render_template('AboutPage.html')

@app.route('/phase-diagram', methods=['POST'])
def phase_diagram():
    try:
        data = request.get_json()
    except Exception as error:
        result = f"Invalid Input"
        return jsonify(result=result) 
    print('phase diagram submission data:',data,file=sys.stderr)
    # Process the data and calculate the result
    #hasError, message = validateData(data)

    try:
        fluid_name = data['fluid_name']
        pressure_unit = data['pressure_output_unit']
        temperature_unit = data['temperature_output_unit']
        image = plot_phase_diagram(fluid_name, pressure_unit, temperature_unit)
        return send_file(image, mimetype='image/png')
    except Exception as error:
        print('error generating phase diagram:',error,file=sys.stderr)
        result = f"{error}"
        return jsonify(result=result)

#compressible orifice solver function
@app.route('/compressible-orifice-calc', methods=['POST'])
def compressible_orifice_calculator():
    try:
        data = request.get_json()
    except Exception as error:
        result = f"Invalid Input"
        return jsonify(result=result) 
    print('orifice calc submission data:',data,file=sys.stderr)
    # Process the data and calculate the result
    #hasError, message = validateData(data)

    try:
        upstream_pressure = unit_correction(data['pressure_unit'])(convertFloat(data,'upstream_pressure')) #applies unit conversion function to upstream pressure based on pressure_unit value
        upstream_temperature = unit_correction(data['temperature_unit'])(convertFloat(data,'upstream_temperature'))
        fluid_name = data['fluid_name']
        downstream_pressure = unit_correction(data['pressure_unit2'])(convertFloat(data,'downstream_pressure'))
        output_type = data['output_type']
        provided_input = data['provided_input']

        if provided_input == 'flowrate':
            flowrate = unit_correction(data['flowrate_unit'])(convertFloat(data,'flowrate'))
            CdA = compressible_CdA(flowrate, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)
            Cv = 38 * CdA #From https://utexas.sharepoint.com/:w:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7B63D8A1D6-CF9C-4E5E-8972-AC06828EC41B%7D&file=User%20Guide.docx&action=default&mobileredirect=true

        elif provided_input == 'diameter':
            Cd = convertFloat(data,'Cd')
            diameter = unit_correction(data['diameter_unit'])(convertFloat(data,'diameter'))
            CdA = (math.pi * ((diameter/2)**2)) * Cd
            flowrate = compressible_flowrate(CdA, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)
            Cv = 38 * CdA #From https://utexas.sharepoint.com/:w:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7B63D8A1D6-CF9C-4E5E-8972-AC06828EC41B%7D&file=User%20Guide.docx&action=default&mobileredirect=true

        elif provided_input == 'CdA':
            CdA = unit_correction(data['CdA_unit'])(convertFloat(data,'CdA'))
            flowrate = compressible_flowrate(CdA, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)
            Cv = 38 * CdA #From https://utexas.sharepoint.com/:w:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7B63D8A1D6-CF9C-4E5E-8972-AC06828EC41B%7D&file=User%20Guide.docx&action=default&mobileredirect=true

        elif provided_input == 'Cv':
            Cv = convertFloat(data,'Cv')
            Cv = in22m2(Cv) #converting Cv input in inches to m for calcs
            CdA = Cv / 38 #From https://utexas.sharepoint.com/:w:/r/sites/TREL/_layouts/15/Doc.aspx?sourcedoc=%7B63D8A1D6-CF9C-4E5E-8972-AC06828EC41B%7D&file=User%20Guide.docx&action=default&mobileredirect=true
            flowrate = compressible_flowrate(CdA, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)

        #returning the selected answer
        if output_type == 'flowrate':
            if data['output_unit'] == 'SCFM':
                flowrate = flowrate / PropsSI('D', 'P', 101325, 'T', 288.15, fluid_name)
                flowrate = flowrate * 60 * 35.3147 #m^3/s to ft^3/min
            else:
                flowrate = flowrate / unit_correction(data['output_unit'])(1)
            result = f"Flowrate: {round(flowrate, 5)}"

        elif output_type == 'CdA':
            CdA = CdA / unit_correction(data['output_unit'])(1)
            result = f"CdA: {round(CdA, 9)}"

        elif output_type == 'Cv':
            Cv = m22in2(Cv) #converts from m^2 to in^2
            result = f"Cv: {round(Cv, 5)}"
    except Exception as error:
        print('error solving compressible orifice:',error,file=sys.stderr)
        result = f"{error}"
        return jsonify(result=result)

    try:
        print('Result:',result,file=sys.stderr)
        jsonify(result=result)
        return jsonify(result=result)
    except Exception as error:
        result = f"Error processing input"
        return jsonify(result=result)
    
#incompressible orifice solver function
@app.route('/incompressible-orifice-calc', methods=['POST'])
def incompressible_orifice_calculator():
    try:
        data = request.get_json()
    except Exception as error:
        result = f"Invalid Input"
        return jsonify(result=result) 
    print('orifice calc submission data:',data,file=sys.stderr)
    # Process the data and calculate the result
    #hasError, message = validateData(data)

    try:
        upstream_pressure = unit_correction(data['pressure_unit'])(convertFloat(data,'upstream_pressure')) #applies unit conversion function to upstream pressure based on pressure_unit value
        upstream_temperature = unit_correction(data['temperature_unit'])(convertFloat(data,'upstream_temperature'))
        fluid_name = data['fluid_name']
        downstream_pressure = unit_correction(data['pressure_unit2'])(convertFloat(data,'downstream_pressure'))
        output_type = data['output_type']
        provided_input = data['provided_input']

        if provided_input == 'flowrate':
            flowrate = unit_correction(data['flowrate_unit'])(convertFloat(data,'flowrate'))
            CdA = incompressible_CdA(flowrate, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)
            Cv = 38 * CdA #Using the sqrt of 2 * the standard density of water to convert between CdA and Cv

        elif provided_input == 'diameter':
            Cd = convertFloat(data,'Cd')
            diameter = unit_correction(data['diameter_unit'])(convertFloat(data,'diameter'))
            CdA = (math.pi * ((diameter/2)**2)) * Cd
            flowrate = incompressible_flowrate(CdA, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)
            Cv = 38 * CdA #Using the sqrt of 2 * the standard density of water to convert between CdA and Cv

        elif provided_input == 'CdA':
            CdA = unit_correction(data['CdA_unit'])(convertFloat(data,'CdA'))
            flowrate = incompressible_flowrate(CdA, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)
            Cv = 38 * CdA #Using the sqrt of 2 * the standard density of water to convert between CdA and Cv

        elif provided_input == 'Cv':
            Cv = convertFloat(data,'Cv')
            Cv = in22m2(Cv) #converting Cv input in inches to m for calcs
            CdA = Cv / 38 #Using the sqrt of 2 * the standard density of water to convert between CdA and Cv
            flowrate = incompressible_flowrate(CdA, upstream_pressure, downstream_pressure, upstream_temperature, fluid_name)

        #returning the selected answer
        if output_type == 'flowrate':
            if data['output_unit'] == 'GPM':
                flowrate = flowrate / PropsSI('D', 'P', upstream_pressure, 'T', upstream_temperature, fluid_name)
                flowrate = flowrate * 60 * 264.172052 #convert m^3/s to GPM
            else:
                flowrate = flowrate / unit_correction(data['output_unit'])(1)
            result = f"Flowrate: {round(flowrate, 5)}"

        elif output_type == 'CdA':
            CdA = CdA / unit_correction(data['output_unit'])(1)
            result = f"CdA: {round(CdA, 9)}"

        elif output_type == 'Cv':
            Cv = m22in2(Cv) #converts from m^2 to in^2
            result = f"Cv: {round(Cv, 5)}"
    except Exception as error:
        print('error solving incompressible orifice:',error,file=sys.stderr)
        result = f"{error}"
        return jsonify(result=result)

    try:
        print('Result:',result,file=sys.stderr)
        jsonify(result=result)
        return jsonify(result=result)
    except Exception as error:
        result = f"Error processing input"
        return jsonify(result=result)
    
# joule thomson solver function
@app.route('/joule-thomson-calc', methods=['POST'])
def joule_thomson_calculator():
    try:
        data = request.get_json()
    except Exception as error:
        result = "Invalid Input"
        return jsonify(result=result) 
    
    print('joule thomson submission data:', data, file=sys.stderr)

    try:
        upstream_pressure = unit_correction(data['pressure_unit'])(convertFloat(data, 'upstream_pressure')) # Applies unit conversion function to upstream pressure based on pressure_unit value
        upstream_temperature = unit_correction(data['temperature_unit'])(convertFloat(data, 'upstream_temperature'))
        fluid_name = data['fluid_name']
        downstream_pressure = unit_correction(data['pressure_unit2'])(convertFloat(data, 'downstream_pressure'))
        
        # Returning the selected answer with two results
        result, coef = joule_thomson(upstream_pressure, upstream_temperature, downstream_pressure, fluid_name)
        result = unit_reversion(data['output_type'])(float(result))
        
    except Exception as error:
        print('error solving joule thomson:', error, file=sys.stderr)
        result = f"{error}"
        return jsonify(result=result)

    try:
        print('Result:', result, file=sys.stderr)
        # Return both result and coefficient in the response
        return jsonify(result=result, coefficient=coef)
    except Exception as error:
        result = "Error processing input"
        return jsonify(result=result)

#property select function
@app.route('/property-select-calc', methods=['POST'])
def property_select_calc():
    try:
        data = request.get_json()
    except Exception as error:
        result = f"Invalid Input"
        return jsonify(result=result) 
    print('orifice calc submission data:',data,file=sys.stderr)
    # Process the data and calculate the result
    #hasError, message = validateData(data)

    try:
        value_1 = unit_correction(data['unit_select_1'])(convertFloat(data,'value_1')) #applies unit conversion function to upstream pressure based on pressure_unit value
        value_2 = unit_correction(data['unit_select_2'])(convertFloat(data,'value_2'))
        fluid_property_1 = data['fluid_property']
        fluid_property_2 = data['fluid_property2']
        output_fluid_property = data['output_fluid_property']
        fluid_name = data['fluid_name']
        result = PropsSI(output_fluid_property, fluid_property_1, value_1, fluid_property_2, value_2, fluid_name)
        if data['unit_select_output'] == 'F':
            result = ((result - 273.15) * 1.8) + 32 
        elif data['unit_select_output'] == 'C':
            result = result - 273.15
        else:
            unit_select_output_function = unit_correction(data['unit_select_output'])
            result = result/unit_select_output_function(1)
        result = f"{round(result, 5)}"

    except Exception as error:
        print('error returning :',error,file=sys.stderr)
        result = f"{error}"
        return jsonify(result=result)

    try:
        print('Result:',result,file=sys.stderr)
        jsonify(result=result)
        return jsonify(result=result)
    except Exception as error:
        result = f"Error processing input"
        return jsonify(result=result)

def unit_correction(unit):
    if unit == 'psia':
        return psi2Pa
    elif unit == 'MPa':
        return MPa2Pa
    elif unit == 'Bar':
        return Bar2Pa
    elif unit == 'R':
        return R2K
    elif unit == 'F':
        return F2K
    elif unit == 'C':
        return C2K
    elif unit == 'in':
        return in2m
    elif unit == 'in^2':
        return in22m2
    elif unit == 'cm^2':
        return cm22m2
    elif unit == 'lb/s':
        return lb2kg
    elif unit == 'lb/ft^3':
        return lbft32kgm3
    elif unit == 'kJ/kg':
        return kJkg2Jkg
    elif unit == 'cal/g':
        return calg2Jkg
    elif unit == 'BTU/lb':
        return BTUlb2Jkg
    elif unit == 'BTU/lbR':
        return BTUlbR2JkgK
    elif unit == 'cP':
        return BTUhrftF2WmK
    elif unit == 'lbs/ft^2':
        return cP2Nsm2
    elif unit == 'BTU/hftF':
        return lbsft22Nsm2
    else:
        return lambda x : x
    
def unit_reversion(unit):
    if unit == 'R':
        return K2R
    elif unit == 'F':
        return K2F
    elif unit == 'C':
        return K2C
    else:
        return lambda x : x
    
def convertFloat(data,fieldName):
    try:
        return float(data[fieldName])
    except:
        raise Exception("Error: " + fieldName + " is not valid")
        
    

if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=True)
