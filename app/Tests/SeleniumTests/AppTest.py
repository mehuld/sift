from selenium import webdriver
from selenium.webdriver.common.by import By  # Importing By class
from selenium.webdriver.support.select import Select
import time
import unittest

class AppTest (unittest.TestCase):
    def setUp(self):
        def get_chrome_driver():
            #print("Getting Chrome Driver")
            options = webdriver.ChromeOptions()
            options.add_argument('--ignore-ssl-errors=yes')
            options.add_argument('--ignore-certificate-errors')

            driver = webdriver.Remote(
                command_executor='http://selenium-container:4444/wd/hub',
                options=options
            )
            return driver

        self.error_range = 0.1
        self.driver = get_chrome_driver()
    
    def test_sift_landingpage_title(self):
        print("test_sift_landingpage_title: Execution Started")
        passed = False
        try:
            self.driver.get("http://sift-container:5000")
            if "Home" in self.driver.title:
                passed = True
                print("test_sift_landingpage_title: Title = Home on SIFT webpage")
        except Exception as error:
            print(error)
        finally:
            #self.driver.close()
            self.driver.quit()
            print("test_sift_landingpage_title: Test Execution Completed")

        self.assertTrue(passed)

    def test_sift_landingpage_title2(self):
        print("test_sift_landingpage_title: Execution Started")
        passed = False
        try:
            self.driver.get("http://sift-container:5000")
            if "Home" in self.driver.title:
                passed = True
                print("test_sift_landingpage_title: Title = Home on SIFT webpage")
        except Exception as error:
            print(error)
        finally:
            #self.driver.close()
            self.driver.quit()
            print("test_sift_landingpage_title: Test Execution Completed")

        self.assertTrue(passed)
    
    def test_compressible_page(self):
        print("test_compressible_page: Execution Started")
        passed = False
        try:
            # Open the webpage where you want to perform the actions
            self.driver.get("http://sift-container:5000")
            print('test_compressible_page: got to home page')
            
            driver = self.driver
            # Find the button element by its ID and click on it
            button = driver.find_element(By.ID, "buttonToCompressibleOrifice")
            button.click()

            # Find the elements and set their values
            driver.find_element(By.ID, 'provided_input').send_keys('flowrate')
            driver.find_element(By.ID, 'upstream_pressure').send_keys('1013250')
            driver.find_element(By.ID, 'upstream_temperature').send_keys('300')
            driver.find_element(By.ID, 'downstream_pressure').send_keys('801325')
            print('test_compressible_page: set values')

            # For dropdowns, you need to use the Select class from Selenium
            fluid_dropdown = Select(driver.find_element(By.ID, 'fluid_name_dropdown'))
            fluid_dropdown.select_by_visible_text('Helium')

            # Set the value for the flowrate
            driver.find_element(By.ID, 'flowrate').send_keys('0.048080791')

            # For dropdowns, use Select class
            output_type_dropdown = Select(driver.find_element(By.ID, 'output_type'))
            output_type_dropdown.select_by_visible_text('Calculate Flowrate')

            # Find the button element by its ID and click on it
            button = driver.find_element(By.ID, "calculateButton")
            button.click()

            # Get Result:
            resultElement = driver.find_element(By.ID, 'result')
            result = resultElement.get_attribute("innerHTML")
            print(result)
            passed = result == 'Result: Flowrate: 0.04808'
        except Exception as error:
            print(error)
        finally:
            self.driver.quit()
            print("test_compressible_page: Test Execution Completed")
        self.assertTrue(passed)
        
def main():
    print("running app tests")
    unittest.main()
if __name__ == "__main__":
    main()
#python app/Tests/SeleniumTests/AppTest.py