import concurrent.futures
import subprocess
import requests
import time

def get_python_command():
    """Determine whether to use python3 or python based on the environment."""
    try:
        # Try to get the version of python3
        result = subprocess.run(["python3", "--version"], capture_output=True, text=True)
        if result.returncode == 0:
            return "python3"
    except FileNotFoundError:
        pass

    try:
        # If python3 is not found, fall back to python
        result = subprocess.run(["python", "--version"], capture_output=True, text=True)
        if result.returncode == 0:
            return "python"
    except FileNotFoundError:
        raise EnvironmentError("Neither python3 nor python was found on this system.")

def wait_for_flask_app():
    url = 'http://sift-container:5000/'
    while True:
        try:
            response = requests.get(url)
            if response.status_code == 200:
                print("Sift is running, tests may begin:")
                break
        except Exception as e:
            print("Waiting for sift to be running...")
            time.sleep(1)

def wait_for_selenium_app():
    url = 'http://selenium-container:4444/'
    while True:
        try:
            response = requests.get(url)
            if response.status_code == 200:
                print("Selenium is running, tests may begin:")
                break
        except Exception as e:
            print("Waiting for selenium to be running...")
            time.sleep(1)

def check_failed_in_tmp_file(file_path):
    try:
        with open(file_path, 'r') as f:
            content = f.read()
            if "FAILED" in content:
                print('Failure in', content)
                return True
            else:
                return False
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return False
    except Exception as e:
        print(f"Error: {e}")
        return False


if __name__ == "__main__":

    python_command = get_python_command()
    #Turn on Sift App
    subprocess.Popen([python_command, "app/app.py"], stdout=open("app/Tests/SeleniumTests/AppLogs.tmp", "w"), stderr=subprocess.STDOUT)

    #Wait for Sift App to be responsive
    wait_for_flask_app()
    wait_for_selenium_app()

    #Run AppTests
    subprocess.run([python_command, "app/Tests/SeleniumTests/AppTest.py"], stdout=open("app/Tests/SeleniumTests/AppTest.tmp", "w"), stderr=subprocess.STDOUT)
    
    if check_failed_in_tmp_file("app/Tests/SeleniumTests/AppTest.tmp"):
        print("Selenium Tests have failed.")
        raise Exception("Selenium Tests have failed.")
    else:
        print("Selenium Tests Passed")    
        result = subprocess.run(['cat', 'app/Tests/SeleniumTests/AppTest.tmp'], capture_output=True, text=True)
        # Check if the command was successful
        if result.returncode == 0:
            # Print the output
            print("Output of AppTest.tmp:")
            print(result.stdout)
        else:
            # Print an error message if the command failed
            print("Error: Failed to execute cat command on AppTest.tmp")